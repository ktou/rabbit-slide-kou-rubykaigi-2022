#!/usr/bin/env ruby

require "adbc"

ADBC::Database.open(driver: "adbc_driver_flight_sql",
                    location: "grpc://127.0.0.1:2929") do |database|
  database.connect do |connection|
    puts(connection..query("SELECT * FROM logs"))
  end
end
