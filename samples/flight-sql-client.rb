#!/usr/bin/env ruby

require "arrow-flight-sql"

location = "grpc://127.0.0.1:2929"
client = ArrowFlight::Client.new(location)
sql_client = ArrowFlightSQL::Client.new(client)
info = sql_client.execute("SELECT * FROM logs")
info.endpoints.each do |endpoint|
  reader = sql_client.do_get(endpoint.ticket)
  puts(reader.read_all)
end
