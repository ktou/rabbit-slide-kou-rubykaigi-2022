#!/usr/bin/env ruby

require "arrow-flight-sql"

class Server < ArrowFlightSQL::Server
  type_register

  def initialize
    super
    @logs = Arrow::Table.new(user: ["alice", "bob", "chris"],
                             action: ["shutdown", "reboot", "boot"])
  end

  private
  def virtual_do_get_flight_info_statement(context, command, descriptor)
    description = ArrowFlight::PathDescriptor.new(["logs"])
    handle = generate_handle(command.query)
    locations = [
      ArrowFlight::Location.new("grpc://127.0.0.1:#{port}"),
    ]
    endpoints = [
      ArrowFlight::Endpoint.new(handle, locations),
    ]
    output = Arrow::ResizableBuffer.new(0)
    @logs.save(output, format: :stream)
    ArrowFlight::Info.new(@logs.schema,
                          descriptor,
                          endpoints,
                          @logs.n_rows,
                          output.size)
  end

  def virtual_do_do_get_statement(context, command)
    ArrowFlight::RecordBatchStream.new(@logs)
  end
end

server = Server.new
server.listen("grpc://127.0.0.1:2929")
gets
